﻿using ApiWebPrueba.Application.UseCases;
using ApiWebPrueba.Application.UseCases.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWebPrueba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonasController : ControllerBase
    {
        //Creando Objeto de tipo interfaz para poder utilizar las funciones del caso de uso.
        private readonly IPersonasUseCase _personasUseCase;

        public PersonasController()
        {
            _personasUseCase = new PersonasUseCase();
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult ListarPersonas()
        {
            try
            {
                var result = _personasUseCase.ListarPersonas();
                if (!result.Succeeded)
                    return BadRequest();
                return new JsonResult(result);
            }
            catch (Exception e)
            {
                //TODO: log error
                return new JsonResult(e.Message);
            }
        }
    }
}
